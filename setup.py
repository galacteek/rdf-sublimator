from setuptools import setup
from setuptools import find_packages


def reqs_parse(path):
    reqs = []
    deps = []

    with open(path) as f:
        lines = f.read().splitlines()
        for line in lines:
            if line.startswith('-e'):
                link = line.split().pop()
                deps.append(link)
            else:
                reqs.append(line)

    return reqs


install_reqs = reqs_parse('requirements.txt')
found_packages = find_packages(exclude=['tests', 'tests.*'])

setup(
    name='rdf-sublimator',
    version='1.1.0',
    license='GPL3',
    author='cipres',
    url='https://gitlab.com/galacteek/rdf-sublimator',
    description='Builds RDF graphs from dbpedia',
    long_description='Builds RDF graphs from dbpedia',
    packages=found_packages,
    install_requires=install_reqs,
    entry_points={
        'console_scripts': [
            'rdf-sublimator = rdf_sublimator:sublimate'
        ]
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
    ],
    keywords=[
        'rdf',
        'sparql'
    ]
)
