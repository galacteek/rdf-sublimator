#!/usr/bin/env python
#
# Simple tool to build RDF graphs by running
# SparQL queries against popular endpoints like dbpedia
#

import io
import os
import os.path
import argparse
import glob
import traceback

from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, RDF


def q(query: str, args):
    prefixes = """
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX dbr: <http://dbpedia.org/resource/>
    PREFIX dbc: <http://dbpedia.org/resource/Category:>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX dc: <http://purl.org/dc/elements/1.1/>
    PREFIX dbpedia2: <http://dbpedia.org/property/>
    PREFIX dbpedia: <http://dbpedia.org/>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX yago:<http://dbpedia.org/class/yago/>
    PREFIX dct: <http://purl.org/dc/terms/>

    PREFIX bd: <http://www.bigdata.com/rdf#>
    PREFIX wikibase: <http://wikiba.se/ontology#>
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    """

    if args.wikidata:
        ep = "https://query.wikidata.org/sparql"
    else:
        ep = args.endpoint

    sparql = SPARQLWrapper(ep)

    if 'dbpedia' in ep:
        sparql.addDefaultGraph("http://dbpedia.org")

    sparql.setQuery(prefixes + query)
    try:
        sparql.setReturnFormat(RDF)
        results = sparql.query()
        return results.convert()
    except Exception:
        traceback.print_exc()


def sublimate():
    parser = argparse.ArgumentParser()
    parser.add_argument("--sparql",
                        "-s",
                        default='*.sparql',
                        metavar='str',
                        dest='qsource',
                        help="SparQL Queries paths regexp")
    parser.add_argument("--ep",
                        "-e",
                        default='https://dbpedia.org/sparql',
                        metavar='str',
                        dest='endpoint',
                        help="SparQL endpoint URL")

    parser.add_argument("--to",
                        "--out",
                        default=None,
                        metavar='str',
                        dest='to',
                        help="Export filepath")

    parser.add_argument("--wikidata",
                        action='store_true',
                        default=False,
                        dest='wikidata')

    args = parser.parse_args()
    g = Graph()

    for path in glob.iglob(args.qsource):
        if not os.path.isfile(path) or not path.endswith('.sparql'):
            continue

        try:
            with open(path, 'rt') as fd:
                query = fd.read()

            qgraph = q(query, args)
            if not qgraph:
                continue

            g += qgraph
        except Exception:
            traceback.print_exc()
            continue

    if args.to is None:
        out = io.BytesIO()
        g.serialize(out, format='ttl')
        out.seek(0, 0)
        print(out.getvalue().decode())
    else:
        with open(args.to, 'w+b') as dst:
            if args.to.endswith('.ttl'):
                g.serialize(dst, format='ttl')
            elif args.to.endswith('.nt'):
                g.serialize(dst, format='nt')
